package com.HuyChu.Angi;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.HuyChu.Angi.customadapter.RelateProductAdapter;
import com.HuyChu.Angi.model.BillDetail;
import com.HuyChu.Angi.model.Product;
import com.HuyChu.Angi.model.Store;
import com.HuyChu.Angi.retrofit.RetrofitUtils;
import com.HuyChu.Angi.retrofit.SOService;
import com.HuyChu.Angi.util.AppProperties;
import com.HuyChu.Angi.util.Util;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductActivity extends AppCompatActivity {
    private LinearLayout linearStore;
    private ImageView imgProductBanner;
    private Button btnOrder;
    private RelateProductAdapter adapter;
    private RecyclerView recyclerView;
    private TextView txtNameProduct, txtCostProduct, txtRating, txtDetail, txtStoreName, txtRatingStore, txtSchedule, txtPhoneStore;
    private Product product;
    private SOService service;
    private Store singleStore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        addControls();
        getData();
        addEvents();
        getRelateProduct();
    }

    private void addEvents() {
        linearStore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProductActivity.this, StoreActivity.class);
                intent.putExtra("store", singleStore);
                finish();
                startActivity(intent);
            }
        });
        btnOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BillDetail billDetail = new BillDetail(product, 1);
                Map<Integer, BillDetail> currentBill = AppProperties.currentBill;
                if (currentBill.containsKey(product.getId())) {
                    BillDetail row = currentBill.get(product.getId());
                    row.setQuantity(row.getQuantity() + 1);
                    currentBill.put(product.getId(), row);
                } else {
                    BillDetail row = new BillDetail(product, 1);
                    currentBill.put(product.getId(), row);
                }
                showSuccessDialog();
            }
        });
    }

    private void getData() {
        Intent intent = getIntent();
        if (!intent.hasExtra("product")) {
            Toast.makeText(ProductActivity.this, "Không tìm thấy sản phẩm", Toast.LENGTH_LONG).show();
        }
        this.product = (Product) intent.getSerializableExtra("product");
        service = RetrofitUtils.getSOSoService();
        loadStore(product.getStoreCode());
        setProductContent(this.product);
    }

    private void loadStore(int storeCode) {
        service.getSingleStore(storeCode).enqueue(new Callback<Store>() {
            @Override
            public void onResponse(Call<Store> call, Response<Store> response) {
                if (response.isSuccessful()) {
                    singleStore = response.body();
                    txtStoreName.setText(singleStore.getStoreName());
                    txtRatingStore.setText(Math.round(singleStore.getStoreRating() * 20) + "%");
                    txtSchedule.setText(singleStore.getStoreSchedule());
                    txtPhoneStore.setText(Util.formatPhoneNumber(singleStore.getStorePhone()));
                } else {
                    Log.d("ProductActivity", "Error loaded STORE from API!. Response code: " + response.code());
                }
            }

            @Override
            public void onFailure(Call<Store> call, Throwable t) {
                Log.d("ProductActivity", "Error loaded from API!. Ex: " + t.getMessage());
            }
        });
    }

    private void showSuccessDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.success_dialog);
        dialog.setCancelable(false);
        TextView btnHome = dialog.findViewById(R.id.btnHome);
        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Intent intent = new Intent(ProductActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
        dialog.show();
    }

    private List<Product> getRelateProduct() {
        List<Product> listRelate = new ArrayList<>();
        service.getRelateProduct(Util.getFirstKeywork(product.getProKeyword())).enqueue(new Callback<List<Product>>() {
            @Override
            public void onResponse(Call<List<Product>> call, Response<List<Product>> response) {
                if (response.isSuccessful()) {
                    adapter = new RelateProductAdapter(ProductActivity.this, response.body());
                    recyclerView.setAdapter(adapter);
                } else {
                    Log.d("ProductActivity", "Error loaded 'RelateProduct' from API!. Response code: " + response.code());
                }
            }

            @Override
            public void onFailure(Call<List<Product>> call, Throwable t) {
                Log.d("ProductActivity", "Error loaded 'RelateProduct' from API!. Ex: " + t.getMessage());
            }
        });
        return listRelate;
    }

    private void setProductContent(Product product) {
        txtCostProduct.setText(Util.formatCurrency(product.getProCost()));
        txtNameProduct.setText(product.getProName());
        txtRating.setText(product.getProRating() + "");
        txtDetail.setText(product.getProDetail());
        Picasso.with(ProductActivity.this)
                .load(AppProperties.IMG_BASE_URL + product.getProImg())
                .placeholder(R.drawable.loading)
                .error(R.drawable.error)
                .into(imgProductBanner);
    }

    private void addControls() {
        linearStore = findViewById(R.id.linearStore);
        btnOrder = findViewById(R.id.btnOrder);
        txtDetail = findViewById(R.id.txtDetail);
        txtNameProduct = findViewById(R.id.txtNameProduct);
        txtCostProduct = findViewById(R.id.txtCostProduct);
        txtRating = findViewById(R.id.txtRating);
        txtStoreName = findViewById(R.id.txtStoreName);
        txtStoreName = findViewById(R.id.txtStoreName);
        txtRatingStore = findViewById(R.id.txtRatingStore);
        txtSchedule = findViewById(R.id.txtSchedule);
        txtPhoneStore = findViewById(R.id.txtPhoneStore);
        imgProductBanner = findViewById(R.id.imgProductBanner);
        recyclerView = findViewById(R.id.recyclerViewRelated);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ProductActivity.this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
    }

    public void ratingProduct(View view) {
        if (!Util.getInfoFromMemmory(this).getEmail().isEmpty()){
            showSuccessRatingDialog();
        }
    }

    private void showSuccessRatingDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.rating_dialog);
        dialog.setCancelable(true);
        TextView btnRating = dialog.findViewById(R.id.btnRating);
        final RatingBar ratingBar = dialog.findViewById(R.id.ratingBar);
        btnRating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JsonObject requestObj = new JsonObject();
                requestObj.addProperty("product_id", product.getId());
                requestObj.addProperty("product_rating", ratingBar.getRating());
                service.rateProduct(requestObj).enqueue(new Callback<Boolean>() {
                    @Override
                    public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                        if (response.isSuccessful()) {
                            Toast.makeText(ProductActivity.this, "Cám ơn bạn đã đánh giá!", Toast.LENGTH_LONG).show();
                        } else {
                            Log.e("ProductActivity", "Rating fail. Code:" + response.code());
                        }
                    }

                    @Override
                    public void onFailure(Call<Boolean> call, Throwable t) {
                        Log.e("ProductActivity", "Rating fail. Ex:", t);
                    }
                });
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(ProductActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
