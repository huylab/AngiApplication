package com.HuyChu.Angi.ui.store;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.HuyChu.Angi.R;
import com.HuyChu.Angi.customadapter.GridProductAdapter;
import com.HuyChu.Angi.customadapter.RelateProductAdapter;
import com.HuyChu.Angi.model.Product;
import com.HuyChu.Angi.model.Store;
import com.HuyChu.Angi.retrofit.RetrofitUtils;
import com.HuyChu.Angi.retrofit.SOService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StoreProductFragment extends Fragment {
    private View root;
    private RelateProductAdapter relateAdapter;
    private GridProductAdapter gridAdapter;
    private RecyclerView rccTopProduct, rccAllProduct;
    private Store currentStore;
    private SOService service;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_storeproduct, container, false);
        addControls();
        renderProduct();
        return root;
    }

    private void renderProduct() {
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            currentStore = (Store) bundle.getSerializable("store");
        }
        service.getProductOfStore(currentStore.getStoreCode()).enqueue(new Callback<List<Product>>() {
            @Override
            public void onResponse(Call<List<Product>> call, Response<List<Product>> response) {
                if (response.isSuccessful()) {
                    gridAdapter = new GridProductAdapter(root.getContext(), (ArrayList<Product>) response.body());
                    gridAdapter.notifyDataSetChanged();
                    rccAllProduct.setAdapter(gridAdapter);
                } else {
                    Log.d("StoreProductFragment", "Load 'ProductOfStore' fail. Code: " + response.code());
                }
            }

            @Override
            public void onFailure(Call<List<Product>> call, Throwable t) {
                Log.d("StoreProductFragment", "Load 'ProductOfStore' fail. Ex: " + t.getMessage());
            }
        });
        service.getTopProductOfStore(currentStore.getStoreCode()).enqueue(new Callback<List<Product>>() {
            @Override
            public void onResponse(Call<List<Product>> call, Response<List<Product>> response) {
                if (response.isSuccessful()) {
                    relateAdapter = new RelateProductAdapter(root.getContext(), response.body());
                    relateAdapter.notifyDataSetChanged();
                    rccTopProduct.setAdapter(relateAdapter);
                } else {
                    Log.d("StoreProductFragment", "Load 'TopProductOfStore' fail. Code: " + response.code());
                }
            }

            @Override
            public void onFailure(Call<List<Product>> call, Throwable t) {
                Log.d("StoreProductFragment", "Load 'TopProductOfStore' fail. Ex: " + t.getMessage());
            }
        });
    }

    private void addControls() {
        service = RetrofitUtils.getSOSoService();
        rccTopProduct = root.findViewById(R.id.rccTopProduct);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(root.getContext(), LinearLayoutManager.HORIZONTAL, false);
        rccTopProduct.setLayoutManager(linearLayoutManager);
        rccAllProduct = root.findViewById(R.id.rccAllProduct);
        GridLayoutManager layoutManager = new GridLayoutManager(root.getContext(), 2);
        rccAllProduct.setLayoutManager(layoutManager);
    }
}

