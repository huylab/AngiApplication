package com.HuyChu.Angi.ui.customer;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.HuyChu.Angi.LoginActivity;
import com.HuyChu.Angi.R;
import com.HuyChu.Angi.model.BillDetail;
import com.HuyChu.Angi.model.Customer;
import com.HuyChu.Angi.model.FacebookResponse;
import com.HuyChu.Angi.model.LoginModel;
import com.HuyChu.Angi.retrofit.RetrofitUtils;
import com.HuyChu.Angi.retrofit.SOService;
import com.HuyChu.Angi.util.CircleTransform;
import com.HuyChu.Angi.util.Util;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomerFragment extends Fragment {
    private TextView txtCusCreate, txtCusEmail, btnLogin, btnUpdate;
    private EditText txtCusName, txtCusPhone, txtCusAddress;
    private ImageView imgCustomer;
    private View view;
    private SOService service;
    private boolean stateLogin = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_customer, container, false);
        addControls();
        addEvents();
        showCustomer();
        return view;
    }

    private void showCustomer() {
        final FacebookResponse facebookResponse = Util.getInfoFromMemmory(getActivity());
        if (!facebookResponse.getEmail().equals("")) {
            JsonObject object = new JsonObject();
            object.addProperty("customer_email", facebookResponse.getEmail());
            service.loginByFacebook(object).enqueue(new Callback<LoginModel>() {
                @Override
                public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
                    if (response.isSuccessful()) {
                        LoginModel model = response.body();
                        loadCustomerInfo(model.getCus()[0], facebookResponse.getImgUrl());
                        saveCustomerId(model.getCus()[0].getCusId());
                    } else {
                        Log.d("CustomerFragment", "Load 'loginByFacebook' fail. Code: " + response.code());
                    }
                }

                @Override
                public void onFailure(Call<LoginModel> call, Throwable t) {
                    Log.d("CustomerFragment", "Load 'loginByFacebook' fail. Ex: ", t);
                }
            });
            stateLogin = true;
            btnLogin.setText("Đăng xuất");
            btnUpdate.setVisibility(View.VISIBLE);
        } else {
            hideView();
            btnLogin.setText("Đăng nhập");
            stateLogin = false;
        }
    }

    private void hideView() {
        txtCusAddress.setVisibility(View.INVISIBLE);
        txtCusCreate.setVisibility(View.INVISIBLE);
        txtCusName.setVisibility(View.INVISIBLE);
        txtCusPhone.setVisibility(View.INVISIBLE);
        txtCusEmail.setVisibility(View.INVISIBLE);
        imgCustomer.setVisibility(View.INVISIBLE);
        btnUpdate.setVisibility(View.INVISIBLE);
    }

    private void saveCustomerId(int cusId) {
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(getString(R.string.com_HuyChu_Angi_CustomerInfo), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("customer_id", cusId);
        editor.commit();
    }

    private void loadCustomerInfo(Customer customer, String urlImage) {
        txtCusAddress.setText(customer.getCusAddress());
        try {
            txtCusCreate.setText(Util.formatDateString(customer.getCreated_at()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        txtCusName.setText(customer.getCusName());
        txtCusPhone.setText(Util.formatPhoneNumber(customer.getCusPhone()));
        txtCusEmail.setText(customer.getCusEmail());
        Picasso.with(view.getContext()).load(urlImage)
                .placeholder(R.drawable.noimage)
                .error(R.drawable.error)
                .transform(new CircleTransform()).into(imgCustomer);
    }

    private void addControls() {
        service = RetrofitUtils.getSOSoService();
        txtCusAddress = view.findViewById(R.id.txtCusAddress);
        txtCusName = view.findViewById(R.id.txtCusName);
        txtCusEmail = view.findViewById(R.id.txtCusEmail);
        txtCusPhone = view.findViewById(R.id.txtCusPhone);
        txtCusCreate = view.findViewById(R.id.txtCusCreate);
        btnLogin = view.findViewById(R.id.btnLogin);
        btnUpdate = view.findViewById(R.id.btnUpdate);
        imgCustomer = view.findViewById(R.id.imgCustomer);
    }

    private void addEvents() {
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (stateLogin) {
                    Util.deleteInfoInMemory(getActivity());
                    new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/", null, HttpMethod.DELETE, new GraphRequest
                            .Callback() {
                        @Override
                        public void onCompleted(GraphResponse graphResponse) {
                            LoginManager.getInstance().logOut();
                        }
                    }).executeAsync();
                    Util.setCurrentBill(new HashMap<Integer, BillDetail>());
                    showCustomer();
                } else {
                    Intent intent = new Intent(getContext(), LoginActivity.class);
                    startActivity(intent);
                    getActivity().finish();
                }
            }
        });

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FacebookResponse facebookResponse = Util.getInfoFromMemmory(getActivity());
                final JsonObject requestObj = new JsonObject();
                requestObj.addProperty("customer_id", facebookResponse.getId());
                requestObj.addProperty("customer_name", txtCusName.getText().toString());
                requestObj.addProperty("customer_address", txtCusAddress.getText().toString());
                requestObj.addProperty("customer_phone", txtCusPhone.getText().toString());
                service.setCustomerInfo(requestObj).enqueue(new Callback<LoginModel>() {
                    @Override
                    public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
                        if (response.isSuccessful()) {
                            int code = response.body().getCode();
                            if (code == 123) {
                                showSuccessDialog();
                            }
                        } else {
                            Log.d("CustomerFragment", "Load 'setCustomerInfo' fail. Code: " + response.code());
                        }
                    }

                    @Override
                    public void onFailure(Call<LoginModel> call, Throwable t) {
                        Log.d("CustomerFragment", "Load 'setCustomerInfo' fail. ", t);
                    }
                });
            }
        });
    }

    private void showSuccessDialog() {
        final Dialog dialog = new Dialog(view.getContext());
        dialog.setContentView(R.layout.update_dialog);
        dialog.setCancelable(true);
        TextView btnHome = dialog.findViewById(R.id.btnHome);
        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                showCustomer();
            }
        });
        dialog.show();
        view.clearFocus();
    }
}