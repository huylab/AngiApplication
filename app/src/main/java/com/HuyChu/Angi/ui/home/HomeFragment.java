package com.HuyChu.Angi.ui.home;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.HuyChu.Angi.R;
import com.HuyChu.Angi.customadapter.ProductAdapter;
import com.HuyChu.Angi.model.Product;
import com.HuyChu.Angi.retrofit.RetrofitUtils;
import com.HuyChu.Angi.retrofit.SOService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends Fragment {
    private ProductAdapter adapter;
    private RecyclerView rccProduct;
    private EditText txtSearch;
    private SOService service;
    private Button btnClearSearch;
    private View root;

    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_home, container, false);
        addControls();
        loadProduct();
        addEvents();
        return root;
    }

    private void loadProduct() {
        service.getAllProduct().enqueue(new Callback<List<Product>>() {
            @Override
            public void onResponse(Call<List<Product>> call, Response<List<Product>> response) {
                if (response.isSuccessful()) {
                    adapter = new ProductAdapter(root.getContext(), (ArrayList<Product>) response.body());
                    adapter.notifyDataSetChanged();
                    rccProduct.setAdapter(adapter);
                } else {
                    Log.d("HomeFragment", "Load 'AllProduct' fail. Code: " + response.code());
                }
            }

            @Override
            public void onFailure(Call<List<Product>> call, Throwable t) {
                Log.d("HomeFragment", "Load 'AllProduct' fail. Code: " + t.getMessage());
            }
        });
    }

    private void addEvents() {
        txtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                HomeFragment.this.adapter.getFilter().filter(charSequence);
                btnClearSearch.setVisibility(View.VISIBLE);
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
        btnClearSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtSearch.setText("");
                txtSearch.clearFocus();
                btnClearSearch.setVisibility(View.INVISIBLE);
                ((InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(root.getWindowToken(),0);
            }
        });
        rccProduct.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                ((InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(root.getWindowToken(),0);
                return false;
            }
        });
    }

    private void addControls() {
        btnClearSearch = root.findViewById(R.id.btnClearSearch);
        txtSearch = root.findViewById(R.id.txtSearch);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(root.getContext());
        rccProduct = root.findViewById(R.id.rccProduct);
        rccProduct.setLayoutManager(linearLayoutManager);
        service = RetrofitUtils.getSOSoService();
    }
}
