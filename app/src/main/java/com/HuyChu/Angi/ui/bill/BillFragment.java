package com.HuyChu.Angi.ui.bill;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.HuyChu.Angi.R;
import com.HuyChu.Angi.customadapter.TabAdapter;

public class BillFragment extends Fragment {
    TabLayout tabLayout;
    TabAdapter adapter;
    ViewPager viewPager;
    View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_bill, container, false);
        addControllers();
        addEvents();
        return view;
    }

    private void addEvents() {
    }

    private void addControllers() {
        viewPager = view.findViewById(R.id.viewPager);
        tabLayout = view.findViewById(R.id.tabLayout);
        adapter = new TabAdapter(getFragmentManager());
        adapter.addFragment(new CurrentBillFragment(), "Đơn hàng hiện tại");
        adapter.addFragment(new ListBillFragment(), "Lịch sử");
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }
}
