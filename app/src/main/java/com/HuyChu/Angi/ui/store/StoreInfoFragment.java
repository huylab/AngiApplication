package com.HuyChu.Angi.ui.store;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.HuyChu.Angi.R;
import com.HuyChu.Angi.model.Store;
import com.HuyChu.Angi.util.Util;

import java.text.ParseException;

public class StoreInfoFragment extends Fragment {
    private View root;
    private Store currentStore;
    private TextView txtStorePhone, txtStoreAddress, txtStoreSchedule, txtStoreCreated, txtStoreDetail;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_storeinfo, container, false);
        addControls();
        setStoreInfo();
        return root;
    }

    private void setStoreInfo() {
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            currentStore = (Store) bundle.getSerializable("store");
        }
        txtStorePhone.setText(currentStore.getStorePhone() + "");
        txtStoreAddress.setText(currentStore.getStoreAddress());
        txtStoreSchedule.setText(currentStore.getStoreSchedule());
        try {
            txtStoreCreated.setText(Util.formatDateString(currentStore.getCreated_at()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        txtStoreDetail.setText(currentStore.getStoreDetail());
    }

    private void addControls() {
        txtStorePhone = root.findViewById(R.id.txtStorePhone);
        txtStoreAddress = root.findViewById(R.id.txtStorePhone);
        txtStoreSchedule = root.findViewById(R.id.txtStoreSchedule);
        txtStoreCreated = root.findViewById(R.id.txtStoreCreated);
        txtStoreDetail = root.findViewById(R.id.txtStoreDetail);
    }
}
