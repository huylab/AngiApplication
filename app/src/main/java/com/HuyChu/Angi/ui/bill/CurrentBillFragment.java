package com.HuyChu.Angi.ui.bill;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.HuyChu.Angi.MainActivity;
import com.HuyChu.Angi.R;
import com.HuyChu.Angi.customadapter.ListBillAdapter;
import com.HuyChu.Angi.model.Bill;
import com.HuyChu.Angi.model.BillDetail;
import com.HuyChu.Angi.model.FacebookResponse;
import com.HuyChu.Angi.retrofit.RetrofitUtils;
import com.HuyChu.Angi.retrofit.SOService;
import com.HuyChu.Angi.util.AppProperties;
import com.HuyChu.Angi.util.Util;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CurrentBillFragment extends Fragment {
    private RecyclerView rccBillDetail;
    private ListBillAdapter adapter;
    private Button btnOrder;
    private View root;
    private SOService service;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_currentbill, container, false);
        addControls(root);
        addEvents();
        return root;
    }

    private void addEvents() {
        btnOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FacebookResponse response = Util.getInfoFromMemmory(getActivity());
                if (response.getId() == 0 ){
                    Toast.makeText(root.getContext(),"Bạn phải đăng nhập để tiếp tục.", Toast.LENGTH_LONG).show();
                } else if(AppProperties.currentBill.size() ==0) {
                    Toast.makeText(root.getContext(),"Vui lòng thêm sản phẩm vào giỏ hàng.", Toast.LENGTH_LONG).show();
                }else {
                    saveBillDetail(response.getId());
                }
            }
        });
    }

    private void saveBillDetail(int customerId) {
        if (customerId != 0) {
            final JsonObject requestObj = new JsonObject();
            requestObj.addProperty("customer_id", customerId);
            JsonArray jsonArray = new JsonArray();
            for (BillDetail detail : AppProperties.currentBill.values()) {
                JsonObject jsonDetail = new JsonObject();
                jsonDetail.addProperty("product_id", detail.getProduct().getId());
                jsonDetail.addProperty("quantity", detail.getQuantity());
                jsonArray.add(jsonDetail);
            }
            requestObj.add("detail", jsonArray);
            service.addBillDetail(requestObj).enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    if (response.isSuccessful()) {
                        Util.setCurrentBill(new HashMap<Integer, BillDetail>());
                        showSuccessDialog();
                        Log.d("CurrentBillFragment", "Load 'addBillDetail' done.");
                    } else {
                        Log.d("CurrentBillFragment", "Load 'addBillDetail' fail. Code:" + response.code());
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    Log.d("CurrentBillFragment", "Load 'addBillDetail' fail.", t);
                }
            });
        }
    }

    private void showSuccessDialog() {
        final Dialog dialog = new Dialog(root.getContext());
        dialog.setContentView(R.layout.add_bill_dialog);
        dialog.setCancelable(false);
        TextView btnHome = dialog.findViewById(R.id.btnHome);
        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Intent intent = new Intent(getActivity(), MainActivity.class);
                getActivity().finish();
                startActivity(intent);
            }
        });
        dialog.show();
    }

    private void saveBill() {
        Double billTotal = 0d;
        for (BillDetail detail : AppProperties.currentBill.values()) {
            billTotal += detail.getProduct().getProCost() * detail.getQuantity();
        }
        FacebookResponse response = Util.getInfoFromMemmory(getActivity());
        JsonObject requestObj = new JsonObject();
        requestObj.addProperty("bill_total", billTotal.intValue());
        requestObj.addProperty("bill_state", "7");
        requestObj.addProperty("customer_id", response.getId());
        Util.deleteBillInMemory(getActivity());
        service.addBill(requestObj).enqueue(new Callback<Bill>() {
            @Override
            public void onResponse(Call<Bill> call, Response<Bill> response) {
                if (response.isSuccessful()) {
                    saveBillDetail(response.body().getBillId());
                } else {
                    Log.d("CurrentBillFragment", "Load 'addBill' fail. Code: " + response.code());
                }
            }

            @Override
            public void onFailure(Call<Bill> call, Throwable t) {
                Log.d("CurrentBillFragment", "Load 'addBill' fail.", t);
            }
        });
    }

    private void addControls(View view) {
        service = RetrofitUtils.getSOSoService();
        btnOrder = view.findViewById(R.id.btnOrder);
    }

    @Override
    public void onPause() {
        rccBillDetail.removeAllViews();
        super.onPause();
    }

    @Override
    public void onResume() {
        adapter = new ListBillAdapter(root.getContext(), AppProperties.currentBill);
        rccBillDetail = root.findViewById(R.id.rccBillDetail);
        LinearLayoutManager layoutManager = new LinearLayoutManager(root.getContext());
        adapter.notifyDataSetChanged();
        rccBillDetail.setAdapter(adapter);
        rccBillDetail.setLayoutManager(layoutManager);
        super.onResume();
    }

}
