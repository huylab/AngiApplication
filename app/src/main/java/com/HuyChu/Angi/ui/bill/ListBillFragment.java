package com.HuyChu.Angi.ui.bill;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.HuyChu.Angi.R;
import com.HuyChu.Angi.customadapter.BillDetailAdapter;
import com.HuyChu.Angi.model.Bill;
import com.HuyChu.Angi.model.BillDetail;
import com.HuyChu.Angi.retrofit.RetrofitUtils;
import com.HuyChu.Angi.retrofit.SOService;
import com.HuyChu.Angi.util.AppProperties;
import com.HuyChu.Angi.util.Util;
import com.google.gson.JsonObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListBillFragment extends Fragment {
    private ArrayList<Bill> historyBills;
    private View root;
    private ListView lstHistory;
    private ArrayAdapter<Bill> adapter;
    private SOService service;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_listbill, container, false);
        addControls();
        addEvents();
        return root;
    }

    private void addEvents() {
        lstHistory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                showDetailDialog(historyBills.get(position));
            }
        });
    }

    private void addControls() {
        service = RetrofitUtils.getSOSoService();
        lstHistory = root.findViewById(R.id.lstHistory);
        loadListBill();
    }

    private void showDetailDialog(Bill bill) {
        final Dialog dialog = new Dialog(root.getContext());
        dialog.setContentView(R.layout.bill_history_dialog);
        dialog.setCancelable(true);
        setContentForDialog(dialog, bill);
        dialog.show();
    }

    private void setContentForDialog(final Dialog dialog, Bill bill) {
        TextView txtIdBill = dialog.findViewById(R.id.txtBillId);
        TextView txtBillState = dialog.findViewById(R.id.txtBillState);
        TextView txtBillCreateDate = dialog.findViewById(R.id.txtBillCreateDate);
        TextView txtBillTotal = dialog.findViewById(R.id.txtBillTotal);
        JsonObject resquestObj = new JsonObject();
        resquestObj.addProperty("bill_id", bill.getBillId());
        service.getBillDetail(resquestObj).enqueue(new Callback<List<BillDetail>>() {
            @Override
            public void onResponse(Call<List<BillDetail>> call, Response<List<BillDetail>> response) {
                if (response.isSuccessful()) {
                    setBillDetails(dialog,response.body());
                } else {
                    Log.d("CustomerFragment", "Load 'getBillDetail' fail. Code: " + response.code());
                }
            }

            @Override
            public void onFailure(Call<List<BillDetail>> call, Throwable t) {
                Log.d("CustomerFragment", "Load 'getBillDetail' fail ", t);
            }
        });
        txtIdBill.setText(bill.getBillId() + "");
        txtBillState.setText(AppProperties.state[bill.getBillState() - 1]);
        try {
            txtBillCreateDate.setText(Util.formatDateString(bill.getCreated_at()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        txtBillTotal.setText(Util.formatCurrency(bill.getBillTotal()) + " VNĐ");
    }

    private void setBillDetails(Dialog dialog, List<BillDetail> lists) {
        ListView lstBillDetail = dialog.findViewById(R.id.lstBillDetail);
        BillDetailAdapter detailAdapter = new BillDetailAdapter(root.getContext(), R.layout.row_history_detail, lists);
        detailAdapter.notifyDataSetChanged();
        lstBillDetail.setAdapter(detailAdapter);
    }

    private void loadListBill() {
        int customerId = Util.getInfoFromMemmory(getActivity()).getId();
        JsonObject requestObj = new JsonObject();
        requestObj.addProperty("customer_id", customerId);
        service.getBillsOfCustomer(requestObj).enqueue(new Callback<List<Bill>>() {
            @Override
            public void onResponse(Call<List<Bill>> call, Response<List<Bill>> response) {
                if (response.isSuccessful()) {
                    adapter = new ArrayAdapter<Bill>(root.getContext(), android.R.layout.simple_list_item_activated_1, response.body());
                    lstHistory.setAdapter(adapter);
                    historyBills = (ArrayList<Bill>) response.body();
                } else {
                    Log.d("CustomerFragment", "Load 'getBillsOfCustomer' fail. Code: " + response.code());
                }
            }

            @Override
            public void onFailure(Call<List<Bill>> call, Throwable t) {
                Log.d("CustomerFragment", "Load 'getBillsOfCustomer' fail.", t);
            }
        });
    }
}
