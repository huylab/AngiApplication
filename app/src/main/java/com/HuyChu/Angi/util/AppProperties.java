package com.HuyChu.Angi.util;

import com.HuyChu.Angi.model.Bill;
import com.HuyChu.Angi.model.BillDetail;
import com.HuyChu.Angi.model.Customer;
import com.HuyChu.Angi.model.FacebookResponse;
import com.HuyChu.Angi.model.Product;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class AppProperties {
    public static FacebookResponse facebookResponse = new FacebookResponse();
    public static String IMG_BASE_URL = "http://api-efood.tech/ANGI/public/image/product/";
    public static int session = 1;
    public static Customer customer = null;
    public static final List<String> keyword = Collections.unmodifiableList(Arrays.asList("Cơm", "Phở", "Trà sữa", "Lẩu", "Nhật Bản", "Hàn Quốc", "Việt Nam", "Huế",
            "Sushi", "Pizza", "Bánh tráng", "Cháo", "Coffee", "Bánh mì", "Kem", "Trái cậy", "Chè", "Bún", "Salat", "Tây", "Món chính", "Món giải khát", "Món tráng miệng", "Món phụ"));
    public static HashMap<Integer, BillDetail> currentBill = new HashMap<>();
    public static final String[] state = {"Hoạt động", "Khóa", "Đăng ký", "Mở cửa", "Đóng cửa",
            "Ngừng kinh doanh", "Đặt hàng", "Đã hủy", "Đang giao hàng", "Đã giao hàng","", "Tạm ngưng"};
}
