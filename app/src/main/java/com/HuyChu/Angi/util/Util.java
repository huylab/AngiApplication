package com.HuyChu.Angi.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.HuyChu.Angi.R;
import com.HuyChu.Angi.model.BillDetail;
import com.HuyChu.Angi.model.FacebookResponse;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class Util {
    public static final List<String> keyword = Collections.unmodifiableList(Arrays.asList("Cơm", "Phở", "Trà sữa", "Lẩu", "Nhật Bản", "Hàn Quốc", "Việt Nam", "Huế",
            "Sushi", "Pizza", "Bánh tráng", "Cháo", "Coffee", "Bánh mì", "Kem", "Trái cậy", "Chè", "Bún", "Salat", "Tây", "Món chính", "Món giải khát", "Món tráng miệng", "Món phụ"));

    public static boolean checkLogin() {
        return AppProperties.customer != null;
    }

    public static void setCurrentBill(HashMap<Integer, BillDetail> billDetail) {
        AppProperties.currentBill = billDetail;
    }

    public static String formatDate(Date date) {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        return format.format(date);
    }

    public static String formatCurrency(Double total) {
        NumberFormat format = NumberFormat.getInstance();
        return format.format(total);
    }

    public static String formatPhoneNumber(Double phone) {
        DecimalFormat df = new DecimalFormat("#");
        return "0" + df.format(phone);
    }

    public static Date formatStringToDate(String date) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return formatter.parse(date);
    }

    public static String formatDateString(String input) throws ParseException {
        Date date = formatStringToDate(input);
        return formatDate(date);
    }

    public static int getFirstKeywork(String keywork) {
        if (keywork.equals("")) return 1;
        String[] array = keywork.split("_");
        return Integer.parseInt(array[0]);
    }

    public static FacebookResponse getInfoFromMemmory(Context context) {
        FacebookResponse model = new FacebookResponse();
        SharedPreferences preferences = context.getSharedPreferences(context.getString(R.string.com_HuyChu_Angi_CustomerInfo), Context.MODE_PRIVATE);
        model.setEmail(preferences.getString("customer-email", ""));
        model.setImgUrl(preferences.getString("picture", ""));
        model.setId(preferences.getInt("customer_id", 0));
        return model;
    }

    public static void deleteInfoInMemory(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(context.getString(R.string.com_HuyChu_Angi_CustomerInfo), Context.MODE_PRIVATE);
        preferences.edit().clear().commit();
        preferences = context.getSharedPreferences(context.getString(R.string.com_HuyChu_Angi_CustomerBill), Context.MODE_PRIVATE);
        preferences.edit().clear().commit();
    }

    public static void saveBillToMemory(int billId, Context context) {
        SharedPreferences preferences = context.getSharedPreferences(context.getString(R.string.com_HuyChu_Angi_CustomerBill), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("bill_id", billId);
        editor.commit();
    }

    public static void deleteBillInMemory(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(context.getString(R.string.com_HuyChu_Angi_CustomerBill), Context.MODE_PRIVATE);
        preferences.edit().clear().apply();
    }

    public static int loadBillFromMemory(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(context.getString(R.string.com_HuyChu_Angi_CustomerBill), Context.MODE_PRIVATE);
        return preferences.getInt("bill_id", 0);
    }
}
