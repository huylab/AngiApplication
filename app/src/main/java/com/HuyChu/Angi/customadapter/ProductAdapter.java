package com.HuyChu.Angi.customadapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.HuyChu.Angi.ProductActivity;
import com.HuyChu.Angi.R;
import com.HuyChu.Angi.model.Product;
import com.HuyChu.Angi.util.AppProperties;
import com.HuyChu.Angi.viewmodel.ProductHolder;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ProductAdapter extends RecyclerView.Adapter<ProductHolder> implements Filterable {
    private Context context;
    private ArrayList<Product> listProduct;
    private ArrayList<Product> searchList;

    public ProductAdapter(Context context, ArrayList<Product> listProduct) {
        this.context = context;
        this.listProduct = listProduct;
        this.searchList = listProduct;
    }

    @Override
    public ProductHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_home, viewGroup, false);
        ProductHolder productHolder = new ProductHolder(v);
        return productHolder;
    }

    @Override
    public void onBindViewHolder(ProductHolder productHolder, int i) {
        final Product product = searchList.get(i);
        productHolder.txtName.setText(product.getProName());
        productHolder.txtDetail.setText(product.getProDetail());
        productHolder.txtRating.setText(product.getProRating() + "");
        if (product.getProSale() > 0) {
            productHolder.imgSale.setVisibility(View.INVISIBLE);
        }
        //Load image to image view
        Picasso.with(context).load(AppProperties.IMG_BASE_URL + product.getProImg())
                .placeholder(R.drawable.loading)
                .error(R.drawable.error)
                .into(productHolder.imgView);
        productHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), ProductActivity.class);
                intent.putExtra("product", product);
                view.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return searchList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String name = charSequence.toString().toLowerCase();
                searchList = new ArrayList<>();
                if (name.isEmpty()) {
                    searchList = listProduct;
                } else {
                    ArrayList<Product> filterList = new ArrayList<>();
                    for (Product product : listProduct) {
                        if (product.getProName().toLowerCase().contains(name)) {
                            filterList.add(product);
                        }
                    }
                    searchList = filterList;
                }
                FilterResults results = new FilterResults();
                results.values = searchList;
                results.count = searchList.size();
                return results;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                searchList = (ArrayList<Product>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
