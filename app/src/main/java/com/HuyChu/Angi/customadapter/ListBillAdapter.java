package com.HuyChu.Angi.customadapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.HuyChu.Angi.R;
import com.HuyChu.Angi.model.BillDetail;
import com.HuyChu.Angi.model.Product;
import com.HuyChu.Angi.util.AppProperties;
import com.HuyChu.Angi.util.Util;
import com.HuyChu.Angi.viewmodel.ListBillHolder;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

public class ListBillAdapter extends RecyclerView.Adapter<ListBillHolder> {
    Context context;
    HashMap<Integer, BillDetail> mapBillDetail;
    List<BillDetail> listBillDetail;

    public ListBillAdapter(Context context, HashMap<Integer, BillDetail> mapBillDetail) {
        this.context = context;
        this.mapBillDetail = mapBillDetail;
        Collection<BillDetail> collection = mapBillDetail.values();
        listBillDetail = new ArrayList<>(collection);
    }

    @Override
    public ListBillHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_bill, viewGroup, false);
        ListBillHolder listBillHolder = new ListBillHolder(v);
        return listBillHolder;
    }


    @Override
    public void onBindViewHolder(ListBillHolder viewHolder, final int position) {
        final BillDetail detail = listBillDetail.get(position);
        final Product product = detail.getProduct();
        viewHolder.txtNameProduct.setText(product.getProName());
        viewHolder.txtQuatity.setText(detail.getQuantity() + "");
        final TextView txtQuatity = viewHolder.txtQuatity;
        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                detail.setQuantity(Integer.parseInt(txtQuatity.getText().toString()));
                AppProperties.currentBill.put(detail.getIdProduct(), detail);
            }
        };
        txtQuatity.addTextChangedListener(textWatcher);
        Picasso.with(context).load(AppProperties.IMG_BASE_URL + product.getProImg()).placeholder(R.drawable.error).placeholder(R.drawable.noimage).into(viewHolder.imgProduct);
        viewHolder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                removerItem(detail);
                Util.setCurrentBill(mapBillDetail);
                return true;
            }
        });
    }

    private void removerItem(BillDetail detail) {
        listBillDetail.remove(detail);
        mapBillDetail.remove(detail.getProduct().getId());
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return listBillDetail.size();
    }
}
