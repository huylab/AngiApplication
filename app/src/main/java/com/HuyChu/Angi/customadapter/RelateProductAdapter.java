package com.HuyChu.Angi.customadapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.HuyChu.Angi.ProductActivity;
import com.HuyChu.Angi.R;
import com.HuyChu.Angi.model.Product;
import com.HuyChu.Angi.viewmodel.RelateHolder;
import com.squareup.picasso.Picasso;

import java.util.List;

public class RelateProductAdapter extends RecyclerView.Adapter<RelateHolder> {
    Context context;
    List<Product> listProduct = null;

    public RelateProductAdapter(Context context, List<Product> listProduct) {
        this.context = context;
        this.listProduct = listProduct;
    }

    @Override
    public RelateHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_relate, viewGroup, false);
        RelateHolder holder = new RelateHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(RelateHolder relateHolder, final int i) {
        Product product = listProduct.get(i);
        Picasso.with(context).load("http://api-efood.tech/ANGI/public/image/product/" + product.getProImg())
                .placeholder(R.drawable.noimage)
                .error(R.drawable.error)
                .into(relateHolder.imgRelateProduct);
        relateHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), ProductActivity.class);
                intent.putExtra("product", listProduct.get(i));
                view.getContext().startActivity(intent);
            }
        });
        relateHolder.txtNameProduct.setText(product.getProName());
    }

    @Override
    public int getItemCount() {
        return listProduct.size();
    }
}
