package com.HuyChu.Angi.customadapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.HuyChu.Angi.ProductActivity;
import com.HuyChu.Angi.R;
import com.HuyChu.Angi.model.Product;
import com.HuyChu.Angi.util.Util;
import com.HuyChu.Angi.viewmodel.GridHolder;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import static com.HuyChu.Angi.util.AppProperties.IMG_BASE_URL;

public class GridProductAdapter extends RecyclerView.Adapter<GridHolder> {
    private ArrayList<Product> listProduct;
    Context context;

    public GridProductAdapter(Context context, ArrayList<Product> listProduct) {
        this.context = context;
        this.listProduct = listProduct;
    }

    @Override
    public GridHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(this.context).inflate(R.layout.grid_item, viewGroup, false);
        GridHolder holder = new GridHolder(view);
        return holder;
    }

    public void onBindViewHolder(GridHolder viewHolder, final int position) {
        final Product product = listProduct.get(position);
        Picasso.with(context).load(IMG_BASE_URL + product.getProImg())
                .placeholder(R.drawable.noimage)
                .error(R.drawable.error)
                .into(viewHolder.imgProduct);
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), ProductActivity.class);
                intent.putExtra("product", product);
                view.getContext().startActivity(intent);
            }
        });
        if (product.getProSale() > 0) {
            viewHolder.imgSale.setVisibility(View.INVISIBLE);
        }
        viewHolder.txtNameProduct.setText(product.getProName());
        viewHolder.txtCostProduct.setText(Util.formatCurrency(product.getProCost()));
        viewHolder.txtRating.setText(product.getProRating() + "");
    }

    @Override
    public int getItemCount() {
        return listProduct.size();
    }
}
