package com.HuyChu.Angi.customadapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.HuyChu.Angi.R;
import com.HuyChu.Angi.model.BillDetail;
import com.HuyChu.Angi.retrofit.RetrofitUtils;
import com.HuyChu.Angi.retrofit.SOService;

import java.util.List;

public class BillDetailAdapter extends BaseAdapter {
    private List<BillDetail> listDetails;
    private Context context;
    private SOService service;
    private TextView txtQuatity, txtNameProduct;
    private int idLayout;

    public BillDetailAdapter(Context context, int idLayout, List<BillDetail> billDetails) {
        this.context = context;
        this.listDetails = billDetails;
        this.idLayout = idLayout;
        service = RetrofitUtils.getSOSoService();
    }

    @Override
    public int getCount() {
        if (listDetails.size() != 0 && !listDetails.isEmpty()) {
            return listDetails.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(idLayout, parent, false);
        }
        txtQuatity = convertView.findViewById(R.id.txtQuatity);
        txtNameProduct = convertView.findViewById(R.id.txtNameProduct);
        txtQuatity.setText(listDetails.get(position).getQuantity() + "");
        txtNameProduct.setText(listDetails.get(position).getProduct().getProName());
        return convertView;
    }
}
