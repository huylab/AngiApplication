package com.HuyChu.Angi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BillDetail {
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("bill_id")
    @Expose
    private int billId;
    @SerializedName("quantity")
    @Expose
    private int quantity;
    @SerializedName("product_id")
    @Expose
    private int idProduct;
    @SerializedName("product")
    @Expose
    private Product product;

    public BillDetail(int billId, int quantity, int idProduct) {
        this.id = 0;
        this.billId = billId;
        this.quantity = quantity;
        this.idProduct = idProduct;
        this.product = null;
    }

    public BillDetail(int id, int billId, int idProduct, int quantity) {
        this.id = id;
        this.billId = billId;
        this.idProduct = idProduct;
        this.quantity = quantity;
        this.product = null;
    }

    public BillDetail(Product product, int quantity) {
        this.id = 0;
        this.billId = 0;
        this.quantity = quantity;
        this.product = product;
        this.idProduct = product.getId();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBillId() {
        return billId;
    }

    public void setBillId(int billId) {
        this.billId = billId;
    }

    public int getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(int idProduct) {
        this.idProduct = idProduct;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
