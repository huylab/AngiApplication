package com.HuyChu.Angi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Store implements Serializable {
    @SerializedName("store_code")
    @Expose
    private int storeCode;
    @SerializedName("store_name")
    @Expose
    private String storeName;
    @SerializedName("store_state")
    @Expose
    private String storeState;
    @SerializedName("store_bank")
    @Expose
    private double storeBank;
    @SerializedName("id_card")
    @Expose
    private int idCard;
    @SerializedName("store_phone")
    @Expose
    private double storePhone;
    @SerializedName("store_address")
    @Expose
    private String storeAddress;
    @SerializedName("store_img")
    @Expose
    private String storeImg;
    @SerializedName("store_detail")
    @Expose
    private String storeDetail;
    @SerializedName("store_rating")
    @Expose
    private int storeRating;
    @SerializedName("store_schedule")
    @Expose
    private String storeSchedule;
    @SerializedName("created_at")
    @Expose
    private String created_at;


    public Store() {
        this.storeCode = 0;
        this.storeName = "";
        this.storeState = "";
        this.storeBank = 0;
        this.idCard = 0;
        this.storePhone = 0;
        this.storeAddress = "";
        this.storeImg = "";
        this.storeDetail = "";
        this.storeRating = 0;
        this.storeSchedule = "";
        //this.created_at = Calendar.getInstance().getTime();
        this.created_at = "";
    }

    public Store(int storeCode, String storeName, String storeState, double storeBank, int idCard,
                 double storePhone, String storeAddress, String storeImg, String storeDetail, int storeRating, String storeSchedule, String created_at) {
        this.storeCode = storeCode;
        this.storeName = storeName;
        this.storeState = storeState;
        this.storeBank = storeBank;
        this.idCard = idCard;
        this.storePhone = storePhone;
        this.storeAddress = storeAddress;
        this.storeImg = storeImg;
        this.storeDetail = storeDetail;
        this.storeRating = storeRating;
        this.storeSchedule = storeSchedule;
        this.created_at = created_at;
    }

    public int getStoreCode() {
        return storeCode;
    }

    public void setStoreCode(int storeCode) {
        this.storeCode = storeCode;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getStoreState() {
        return storeState;
    }

    public void setStoreState(String storeState) {
        this.storeState = storeState;
    }

    public double getStoreBank() {
        return storeBank;
    }

    public void setStoreBank(double storeBank) {
        this.storeBank = storeBank;
    }

    public int getIdCard() {
        return idCard;
    }

    public void setIdCard(int idCard) {
        this.idCard = idCard;
    }

    public double getStorePhone() {
        return storePhone;
    }

    public void setStorePhone(double storePhone) {
        this.storePhone = storePhone;
    }

    public String getStoreAddress() {
        return storeAddress;
    }

    public void setStoreAddress(String storeAddress) {
        this.storeAddress = storeAddress;
    }

    public String getStoreImg() {
        return storeImg;
    }

    public void setStoreImg(String storeImg) {
        this.storeImg = storeImg;
    }

    public String getStoreDetail() {
        return storeDetail;
    }

    public void setStoreDetail(String storeDetail) {
        this.storeDetail = storeDetail;
    }

    public int getStoreRating() {
        return storeRating;
    }

    public void setStoreRating(int storeRating) {
        this.storeRating = storeRating;
    }

    public String getStoreSchedule() {
        return storeSchedule;
    }

    public void setStoreSchedule(String storeSchedule) {
        this.storeSchedule = storeSchedule;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }
}
