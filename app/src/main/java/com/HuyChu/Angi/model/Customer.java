package com.HuyChu.Angi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Customer {
    @SerializedName("customer_id")
    @Expose
    private int cusId;
    @SerializedName("customer_email")
    @Expose
    private String cusEmail;
    @SerializedName("customer_name")
    @Expose
    private String cusName;
    @SerializedName("customer_phone")
    @Expose
    private double cusPhone;
    @SerializedName("customer_address")
    @Expose
    private String cusAddress;
    @SerializedName("customer_rating")
    @Expose
    private int cusRating;
    @SerializedName("customer_keyword")
    @Expose
    private String cusKeyword;
    @SerializedName("created_at")
    @Expose
    private String created_at;

    public Customer(int cusId, String cusEmail, String cusName, double cusPhone, String cusAddress, int cusRating, String cusKeyword, String created_at) {
        this.cusId = cusId;
        this.cusEmail = cusEmail;
        this.cusName = cusName;
        this.cusPhone = cusPhone;
        this.cusAddress = cusAddress;
        this.cusRating = cusRating;
        this.cusKeyword = cusKeyword;
        this.created_at = created_at;
    }

    public Customer(String cusEmail, String cusName, double cusPhone, String cusAddress, int cusRating, String cusKeyword, String created_at) {
        this.cusEmail = cusEmail;
        this.cusName = cusName;
        this.cusPhone = cusPhone;
        this.cusAddress = cusAddress;
        this.cusRating = cusRating;
        this.cusKeyword = cusKeyword;
        this.created_at = created_at;
    }

    public int getCusId() {
        return cusId;
    }

    public void setCusId(int cusId) {
        this.cusId = cusId;
    }

    public String getCusEmail() {
        return cusEmail;
    }

    public void setCusEmail(String cusEmail) {
        this.cusEmail = cusEmail;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public double getCusPhone() {
        return cusPhone;
    }

    public void setCusPhone(double cusPhone) {
        this.cusPhone = cusPhone;
    }

    public String getCusAddress() {
        return cusAddress;
    }

    public void setCusAddress(String cusAddress) {
        this.cusAddress = cusAddress;
    }

    public int getCusRating() {
        return cusRating;
    }

    public void setCusRating(int cusRating) {
        this.cusRating = cusRating;
    }

    public String getCusKeyword() {
        return cusKeyword;
    }

    public void setCusKeyword(String cusKeyword) {
        this.cusKeyword = cusKeyword;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }
}
