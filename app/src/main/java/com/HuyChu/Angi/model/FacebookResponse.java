package com.HuyChu.Angi.model;

import java.io.Serializable;

public class FacebookResponse implements Serializable {
    private String email;
    private String imgUrl;
    private int id;

    public FacebookResponse() {
        this.email = "";
        this.imgUrl = "";
        this.id = 0;
    }

    public FacebookResponse(String email, String imgUrl, int id) {
        this.email = email;
        this.imgUrl = imgUrl;
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }
}
