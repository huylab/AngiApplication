package com.HuyChu.Angi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginModel {
    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("mess")
    @Expose
    private String mess;
    @SerializedName("cus")
    @Expose
    private Customer[] cus = null;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMess() {
        return mess;
    }

    public void setMess(String mess) {
        this.mess = mess;
    }

    public Customer[] getCus() {
        return cus;
    }

    public void setCus(Customer[] cus) {
        this.cus = cus;
    }
}
