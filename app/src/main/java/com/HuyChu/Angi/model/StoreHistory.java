package com.HuyChu.Angi.model;

import java.util.Date;

public class StoreHistory {
    private int id;
    private int storeCode;
    private Date startDate;
    private Date endDate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStoreCode() {
        return storeCode;
    }

    public void setStoreCode(int storeCode) {
        this.storeCode = storeCode;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public StoreHistory(int id, int storeCode, Date startDate, Date endDate) {
        this.id = id;
        this.storeCode = storeCode;
        this.startDate = startDate;
        this.endDate = endDate;
    }
}
