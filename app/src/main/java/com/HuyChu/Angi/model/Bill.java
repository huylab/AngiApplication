package com.HuyChu.Angi.model;

import com.HuyChu.Angi.util.AppProperties;
import com.HuyChu.Angi.util.Util;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Bill {
    @SerializedName("bill_id")
    @Expose
    private int billId;
    @SerializedName("customer_id")
    @Expose
    private int customerId;
    @SerializedName("bill_state")
    @Expose
    private int billState;
    @SerializedName("bill_total")
    @Expose
    private double billTotal;
    @SerializedName("created_at")
    @Expose
    private String created_at;
    private ArrayList<BillDetail> listBillDetail;

    public Bill(int customerId, int billState, double billTotal, ArrayList<BillDetail> listBillDetail, String created_at) {
        this.listBillDetail = listBillDetail;
        this.customerId = customerId;
        this.billState = billState;
        this.billTotal = billTotal;
        this.created_at = created_at;
    }

    public Bill(int billId, int customerId, int billState, int billTotal, String created_at) {
        this.billId = billId;
        this.listBillDetail = null;
        this.customerId = customerId;
        this.billState = billState;
        this.billTotal = billTotal;
        this.created_at = created_at;
    }


    public Bill(int billId, int customerId, int billState, int billTotal, ArrayList<BillDetail> listBillDetail, String created_at) {
        this.billId = billId;
        this.listBillDetail = listBillDetail;
        this.customerId = customerId;
        this.billState = billState;
        this.billTotal = billTotal;
        this.created_at = created_at;
    }

    public Bill() {
        this.billId = 0;
        this.listBillDetail = null;
        this.customerId = 0;
        this.billState = 0;
        this.billTotal = 0d;
        this.created_at = "";
    }

    public ArrayList<BillDetail> getListBillDetail() {
        return listBillDetail;
    }

    public void setListBillDetail(ArrayList<BillDetail> listBillDetail) {
        this.listBillDetail = listBillDetail;
    }

    public int getBillId() {
        return billId;
    }

    public void setBillId(int billId) {
        this.billId = billId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public int getBillState() {
        return billState;
    }

    public void setBillState(int billState) {
        this.billState = billState;
    }

    public double getBillTotal() {
        return billTotal;
    }

    public void setBillTotal(double billTotal) {
        this.billTotal = billTotal;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    @Override
    public String toString() {
        return "Mã đơn: " + billId + " -- " + AppProperties.state[billState - 1] + " -- " + Util.formatCurrency(billTotal) + " VNĐ";
    }
}
