package com.HuyChu.Angi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Product implements Serializable {
    @SerializedName("product_id")
    @Expose
    private int id;
    @SerializedName("store_code")
    @Expose
    private int storeCode;
    @SerializedName("product_name")
    @Expose
    private String proName;
    @SerializedName("product_cost")
    @Expose
    private double proCost;
    @SerializedName("product_img")
    @Expose
    private String proImg;
    @SerializedName("product_detail")
    @Expose
    private String proDetail;
    @SerializedName("product_state")
    @Expose
    private int proState;
    @SerializedName("product_keyword")
    @Expose
    private String proKeyword;
    @SerializedName("product_rating")
    @Expose
    private double proRating;
    @SerializedName("product_sale")
    @Expose
    private int proSale;


    public Product(int id, int storeCode, String proName, double proCost, String proImg,
                   String proDetail, int proState, String proKeyword, double proRating, int proSale) {
        this.id = id;
        this.storeCode = storeCode;
        this.proName = proName;
        this.proCost = proCost;
        this.proImg = proImg;
        this.proDetail = proDetail;
        this.proState = proState;
        this.proKeyword = proKeyword;
        this.proRating = proRating;
        this.proSale = proSale;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStoreCode() {
        return storeCode;
    }

    public void setStoreCode(int storeCode) {
        this.storeCode = storeCode;
    }

    public String getProName() {
        return proName;
    }

    public void setProName(String proName) {
        this.proName = proName;
    }

    public double getProCost() {
        return proCost;
    }

    public void setProCost(double proCost) {
        this.proCost = proCost;
    }

    public String getProImg() {
        return proImg;
    }

    public void setProImg(String proImg) {
        this.proImg = proImg;
    }

    public String getProDetail() {
        return proDetail;
    }

    public void setProDetail(String proDetail) {
        this.proDetail = proDetail;
    }

    public int getProState() {
        return proState;
    }

    public void setProState(int proState) {
        this.proState = proState;
    }

    public String getProKeyword() {
        return proKeyword;
    }

    public void setProKeyword(String proKeyword) {
        this.proKeyword = proKeyword;
    }

    public double getProRating() {
        return proRating;
    }

    public void setProRating(double proRating) {
        this.proRating = proRating;
    }

    public int getProSale() {
        return proSale;
    }

    public void setProSale(int proSale) {
        this.proSale = proSale;
    }
}
