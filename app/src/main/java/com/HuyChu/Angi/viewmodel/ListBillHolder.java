package com.HuyChu.Angi.viewmodel;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.HuyChu.Angi.R;

public class ListBillHolder extends RecyclerView.ViewHolder {
    public ImageView imgAdd, imgRemove, imgProduct;
    public TextView txtQuatity, txtNameProduct;

    public ListBillHolder(View itemView) {
        super(itemView);
        this.imgAdd = itemView.findViewById(R.id.imgAdd);
        this.imgRemove = itemView.findViewById(R.id.imgRemove);
        this.imgProduct = itemView.findViewById(R.id.imgProduct);
        this.txtQuatity = itemView.findViewById(R.id.txtQuatity);
        this.txtNameProduct = itemView.findViewById(R.id.txtNameProduct);
        addEvents();
    }

    private void addEvents() {
        imgAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int quatity = Integer.parseInt(txtQuatity.getText().toString()) + 1;
                txtQuatity.setText(quatity + "");
            }
        });
        imgRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int quatity = Integer.parseInt(txtQuatity.getText().toString());
                if (quatity > 1) quatity--;
                txtQuatity.setText(quatity + "");
            }
        });
    }
}
