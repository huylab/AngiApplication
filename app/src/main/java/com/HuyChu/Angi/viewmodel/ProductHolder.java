package com.HuyChu.Angi.viewmodel;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.HuyChu.Angi.R;

public class ProductHolder extends RecyclerView.ViewHolder {
    public ImageView imgView, imgSale;
    public TextView txtName, txtRating, txtDetail;

    public ProductHolder(View itemView) {
        super(itemView);
        this.imgView = itemView.findViewById(R.id.imgProduct);
        this.txtName = itemView.findViewById(R.id.txtNameProduct);
        this.txtRating = itemView.findViewById(R.id.txtRating);
        this.txtDetail = itemView.findViewById(R.id.txtDetail);
        this.imgSale = itemView.findViewById(R.id.imgSale);
    }
}
