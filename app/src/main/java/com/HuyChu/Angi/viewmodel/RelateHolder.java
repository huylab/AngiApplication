package com.HuyChu.Angi.viewmodel;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.HuyChu.Angi.R;

public class RelateHolder extends RecyclerView.ViewHolder {
    public ImageView imgRelateProduct;
    public TextView txtNameProduct;

    public RelateHolder(View itemView) {
        super(itemView);
        this.imgRelateProduct = itemView.findViewById(R.id.imgRelateProduct);
        this.txtNameProduct = itemView.findViewById(R.id.txtNameProduct);
    }
}
