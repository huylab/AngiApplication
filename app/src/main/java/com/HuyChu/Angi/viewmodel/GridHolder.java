package com.HuyChu.Angi.viewmodel;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.HuyChu.Angi.R;

public class GridHolder extends RecyclerView.ViewHolder {
    public ImageView imgProduct, imgSale;
    public TextView txtNameProduct, txtCostProduct, txtRating;

    public GridHolder(View itemView) {
        super(itemView);
        imgProduct = itemView.findViewById(R.id.imgProduct);
        imgSale = itemView.findViewById(R.id.imgSale);
        txtRating = itemView.findViewById(R.id.txtRating);
        txtNameProduct = itemView.findViewById(R.id.txtNameProduct);
        txtCostProduct = itemView.findViewById(R.id.txtCostProduct);
    }
}
