package com.HuyChu.Angi;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.HuyChu.Angi.customadapter.TabAdapter;
import com.HuyChu.Angi.model.Store;
import com.HuyChu.Angi.ui.store.StoreInfoFragment;
import com.HuyChu.Angi.ui.store.StoreProductFragment;

public class StoreActivity extends AppCompatActivity {
    private TextView txtStoreName, txtStoreRating;
    private TabLayout tabLayout;
    private TabAdapter adapter;
    private ViewPager viewPager;
    private Store currentStore;
    private LinearLayout lnRating;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getStoreFromIntent();
        addControls();
        addEvents();
    }

    private void getStoreFromIntent() {
        Intent intent = getIntent();
        if (intent.hasExtra("store")) {
            currentStore = (Store) intent.getSerializableExtra("store");
        }
    }

    private void addControls() {
        txtStoreName = findViewById(R.id.txtStoreName);
        txtStoreRating = findViewById(R.id.txtStoreRating);
        lnRating = findViewById(R.id.lnRating);
        viewPager = findViewById(R.id.viewPager);
        tabLayout = findViewById(R.id.tabLayout);
        adapter = new TabAdapter(getSupportFragmentManager());
        txtStoreRating.setText(Math.round(currentStore.getStoreRating() * 20) + "%");
        txtStoreName.setText(currentStore.getStoreName());
        Bundle bundle = new Bundle();
        bundle.putSerializable("store", currentStore);
        StoreInfoFragment storeInfoFragment = new StoreInfoFragment();
        storeInfoFragment.setArguments(bundle);
        StoreProductFragment storeProductFragment = new StoreProductFragment();
        storeProductFragment.setArguments(bundle);
        adapter.addFragment(storeInfoFragment, "Thông tin cửa hàng");
        adapter.addFragment(storeProductFragment, "Sản phẩm");
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void addEvents() {
        lnRating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //show Rating popup
            }
        });
    }
}
