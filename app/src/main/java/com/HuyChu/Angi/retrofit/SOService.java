package com.HuyChu.Angi.retrofit;

import com.HuyChu.Angi.model.Bill;
import com.HuyChu.Angi.model.BillDetail;
import com.HuyChu.Angi.model.Customer;
import com.HuyChu.Angi.model.LoginModel;
import com.HuyChu.Angi.model.Product;
import com.HuyChu.Angi.model.Store;
import com.google.gson.JsonObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface SOService {
    @GET("product/all")
    Call<List<Product>> getAllProduct();

    @GET("product/{idProduct}")
    Call<JsonObject> getSingleProduct(@Path("idProduct") int idProduct);

    @GET("store/{idStore}")
    Call<Store> getSingleStore(@Path("idStore") int idStore);

    @GET("product/relate/{keyword}")
    Call<List<Product>> getRelateProduct(@Path("keyword") int keyword);

    @GET("store/{idStore}/listproduct")
    Call<List<Product>> getProductOfStore(@Path("idStore") int idStore);

    @GET("store/{idStore}/top")
    Call<List<Product>> getTopProductOfStore(@Path("idStore") int idStore);

    /*json input must be { "bill_id": 1 } */
    @POST("bill/detail")
    Call<List<BillDetail>> getBillDetail(@Body JsonObject json);

    /*json input must be { "customer_id": 1 } */
    @POST("bill")
    Call<List<Bill>> getBillsOfCustomer(@Body JsonObject json);

    /*json input must be { "customer_email": 1 } */
    @POST("customer/login")
    Call<LoginModel> loginByFacebook(@Body JsonObject json);

    /*json input must be
        {
            customer_id": "14",
            "customer_name": "Nguyen van hai",
            "customer_phone": "123123123",
            "customer_address": "nha tui chu dau",
            "customer_rating": "1",
            "customer_keyword": "1_2_3_4_5"
        } */
    @POST("customer/set")
    Call<LoginModel> setCustomerInfo(@Body JsonObject customerInfo);

    @POST("bill/add")
    Call<Bill> addBill(@Body JsonObject bill);

    @POST("customer/{idCustomer}")
    Call<Customer> customerInfo(@Path("idCustomer") int idCustomer);

    /*Input {
                "product_id": 88,
                "product_rating": 3
           }*/
    @POST("product")
    Call<Boolean> rateProduct(@Body JsonObject jsonObject);

    /*{
        "bill_id": "3",
        "detail": [
            {
                "product_id": 1,
                "quantity": 12
            },
            {
                "product_id": 1,
                "quantity": 12
            }
        ]
    }*/
    @POST("bill/add/detail")
    Call<JsonObject> addBillDetail(@Body JsonObject jsonObject);
}
