package com.HuyChu.Angi.retrofit;

public class RetrofitUtils {
    public static final String BASE_URL = "http://api-efood.tech/api/";

    public static SOService getSOSoService() {
        return RetrofitClient.getClient(BASE_URL).create(SOService.class);
    }
}
